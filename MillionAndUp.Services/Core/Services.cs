﻿using FluentValidation.Results;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using MillionAndUp.Domain.FluentValidation;
using MillionAndUp.Domain.Interfaces;
using MillionAndUp.Domain.Response;

namespace MillionAndUp.Services.Core
{
    /// <summary>
    /// Helper for response files and rest using mediatr pattern
    /// </summary>
    public class Services : IServices
    {
        /// <summary>
        /// Mediator interface
        /// </summary>
        private readonly IMediator _mediator;

        public Services(IMediator mediator)
        {
            _mediator = mediator;
        }
        /// <summary>
        /// Validate request with fluent validate
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Request"></param>
        /// <returns></returns>
        public async Task<ObjectResult> Execute<T>(T Request) where T : ValidateRequest
        {
            var validationResult = Request.FluentValidateRequest();
            if (!validationResult.IsValid)
            {
                return formatError<T>(validationResult);
            }
            return BuildResponse(await _mediator.Send(Request) as dynamic);
        }
        /// <summary>
        /// Validate request with fluent validate
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Request"></param>
        /// <returns></returns>
        public async Task<IActionResult> FileExecute<T>(T Request) where T : ValidateRequest
        {
            var validationResult = Request.FluentValidateRequest();
            if (!validationResult.IsValid)
            {
                return formatError<T>(validationResult);
            }
            return BuildFileResponse(await _mediator.Send(Request) as (byte[], string, string)?);
        }
        /// <summary>
        /// Build a response
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="response"></param>
        /// <returns></returns>
        private ObjectResult BuildResponse<T>(ResponseModel<T> response)
        {
            ObjectResult result = new ObjectResult(response);
            result.StatusCode = (int)response.StatusCode;
            return result;
        }
        /// <summary>
        /// Build a FileContentResult for download files
        /// </summary>
        /// <param name="response">Tuple (bytes array, content type, fileName)</param>
        /// <returns></returns>
        private IActionResult BuildFileResponse((byte[], string, string)? response)
        {
            if (response is null)
                return new NotFoundResult();
            var result = new FileContentResult(response.Value.Item1.ToArray(), response.Value.Item2);
            result.FileDownloadName = response.Value.Item3;
            return result;
        }
        /// <summary>
        /// Build a response model with a list of errors in the request
        /// </summary>
        /// <typeparam name="T">ValidateRequest class (FluentValidator)</typeparam>
        /// <param name="validationResult">Validation Result of FluentValidator</param>
        /// <returns></returns>
        private ObjectResult formatError<T>(ValidationResult validationResult) where T : ValidateRequest
        {
            var errors = validationResult.Errors.Select(x => new ErrorsList() { PropertyName = x.PropertyName, ErrorMessage = x.ErrorMessage }).ToList();
            var result = new ResponseModel<List<ErrorsList>>(errors);
            return BuildResponse(result);
        }
    }
}
