﻿using MediatR;
using MillionAndUp.Domain.Interfaces;
using MillionAndUp.Domain.Request.Properties;
using MillionAndUp.Domain.Response;
using MillionAndUp.Domain.Response.Properties;

namespace MillionAndUp.Services.Handlers
{
    /// <summary>
    /// Handlers of Properties controller mediatr routes
    /// </summary>
    public class PropertiesHandlers :
        IRequestHandler<GetPropertiesRequest, ResponseModel<List<GetPropertiesResponse>>>,
        IRequestHandler<UpdatePropertyRequest, ResponseModel<GetPropertiesResponse>>,
        IRequestHandler<UpdatePriceRequest, ResponseModel<GetPropertiesResponse>>,
        IRequestHandler<CreatePropertyRequest, ResponseModel<GetPropertiesResponse>>
    {
        private readonly IProperties _properties;

        public PropertiesHandlers(IProperties properties)
        {
            _properties = properties;
        }
        /// <summary>
        /// Get properties route
        /// </summary>
        /// <param name="request">request</param>
        /// <param name="cancellationToken"></param>
        /// <returns>list of properties</returns>
        public async Task<ResponseModel<List<GetPropertiesResponse>>> Handle(GetPropertiesRequest request, CancellationToken cancellationToken) => await _properties.GetProperties(request);
        /// <summary>
        /// Update property
        /// </summary>
        /// <param name="request">request</param>
        /// <param name="cancellationToken"></param>
        /// <returns>updated property</returns>
        public async Task<ResponseModel<GetPropertiesResponse>> Handle(UpdatePropertyRequest request, CancellationToken cancellationToken) => await _properties.UpdateProperty(request);
        /// <summary>
        /// Update price of property
        /// </summary>
        /// <param name="request">request</param>
        /// <param name="cancellationToken"></param>
        /// <returns>property updated</returns>
        public async Task<ResponseModel<GetPropertiesResponse>> Handle(UpdatePriceRequest request, CancellationToken cancellationToken) => await _properties.UpdatePropertyPrice(request);
        /// <summary>
        /// Create a new property
        /// </summary>
        /// <param name="request">request</param>
        /// <param name="cancellationToken"></param>
        /// <returns>property created</returns>
        public async Task<ResponseModel<GetPropertiesResponse>> Handle(CreatePropertyRequest request, CancellationToken cancellationToken) => await _properties.CreateNewProperty(request);
    }
}
