﻿using MediatR;
using MillionAndUp.Domain.Interfaces;
using MillionAndUp.Domain.Request.Properties;
using MillionAndUp.Domain.Response;

namespace MillionAndUp.Services.Handlers
{
    /// <summary>
    /// Handlers for property image request
    /// </summary>
    public class PropertyImagesHandler :
        IRequestHandler<AddImagePropertyRequest, ResponseModel<string>>,
        IRequestHandler<DownloadPropertyImageRequest, (byte[], string, string)?>
    {
        private readonly IPropertyImage _propertyImage;

        public PropertyImagesHandler(IPropertyImage propertyImage)
        {
            _propertyImage = propertyImage;
        }
        /// <summary>
        /// Add new image to property
        /// </summary>
        /// <param name="request">request</param>
        /// <param name="cancellationToken"></param>
        /// <returns>response</returns>
        public async Task<ResponseModel<string>> Handle(AddImagePropertyRequest request, CancellationToken cancellationToken) => await _propertyImage.AddImageToProperty(request);
        /// <summary>
        /// Download Property Image
        /// </summary>
        /// <param name="request">request</param>
        /// <param name="cancellationToken"></param>
        /// <returns>File</returns>
        public async Task<(byte[], string, string)?> Handle(DownloadPropertyImageRequest request, CancellationToken cancellationToken) => await _propertyImage.DownloadPropertyImage(request);
    }
}
