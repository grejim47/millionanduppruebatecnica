﻿using Microsoft.Extensions.DependencyInjection;
using MillionAndUp.Domain.Interfaces;

namespace MillionAndUp.Services
{
    public static class DependencyInjection
    {
        /// <summary>
        /// add services dependencys
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(typeof(DependencyInjection).Assembly));
            services.AddScoped<IServices, Core.Services>();
            return services;
        }
    }
}