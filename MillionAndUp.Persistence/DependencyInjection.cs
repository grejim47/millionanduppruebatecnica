﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MillionAndUp.Domain.Interfaces;
using MillionAndUp.Persistence.Context;
using MillionAndUp.Persistence.Methods;

namespace MillionAndUp.Persistence
{
    public static class DependencyInjection
    {
        /// <summary>
        /// Add persistence services
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddPersistence(this IServiceCollection services)
        {
            string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\DB\DB.mdf;Integrated Security=True";
            services.AddDbContext<PropertiesContext>(options => options.UseSqlServer(connectionString));
            services.AddTransient<IPropertiesPersistence, PropertiesMethods>();
            services.AddTransient<IPropertyImagePersistence, PropertyImageMethods>();
            return services;
        }
    }
}