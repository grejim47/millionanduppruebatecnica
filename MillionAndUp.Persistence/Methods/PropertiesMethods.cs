﻿using Microsoft.EntityFrameworkCore;
using MillionAndUp.Domain.DBEntities;
using MillionAndUp.Domain.Interfaces;
using MillionAndUp.Domain.Request;
using MillionAndUp.Persistence.Context;

namespace MillionAndUp.Persistence.Methods
{
    /// <summary>
    /// Properties methods
    /// </summary>
    public class PropertiesMethods : IPropertiesPersistence
    {
        private readonly PropertiesContext _context;

        public PropertiesMethods(PropertiesContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Get properties by filters
        /// </summary>
        /// <param name="filters">filters</param>
        /// <returns>List of properties</returns>
        public async Task<List<Property>> GetProperties(List<Filters> filters)
        {
            return await _context.Properties.Include(x => x.Owner).Include(x => x.PropertyTraces).Include(x => x.PropertyImages).Filter(filters).ToListAsync();
        }
        /// <summary>
        /// Get property by Id
        /// </summary>
        /// <param name="idProperty">id property</param>
        /// <returns>Property</returns>
        public async Task<Property> GetPropertyById(int idProperty)
        {
            return await _context.Properties.Include(x => x.Owner).Include(x => x.PropertyTraces).Include(x => x.PropertyImages).Where(x => x.IdProperty == idProperty).FirstOrDefaultAsync();
        }
        /// <summary>
        /// Update property
        /// </summary>
        /// <param name="propertyToUpdate">Property to update</param>
        /// <returns></returns>
        public async Task UpdateProperty(Property propertyToUpdate)
        {
            _context.Update(propertyToUpdate);
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// Create property
        /// </summary>
        /// <param name="newProperty">newProperty</param>
        /// <returns></returns>
        public async Task AddNewProperty(Property newProperty)
        {
            await _context.AddAsync(newProperty);
            await _context.SaveChangesAsync();
        }
    }
}
