﻿using Microsoft.EntityFrameworkCore;
using MillionAndUp.Domain.DBEntities;
using MillionAndUp.Domain.Interfaces;
using MillionAndUp.Persistence.Context;

namespace MillionAndUp.Persistence.Methods
{
    /// <summary>
    /// Methods for PropertyImage entity
    /// </summary>
    public class PropertyImageMethods : IPropertyImagePersistence
    {
        private readonly PropertiesContext _context;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context">BD context</param>
        public PropertyImageMethods(PropertiesContext context)
        {
            _context = context;
        }
        /// <summary>
        /// AddImage to property
        /// </summary>
        /// <param name="newImage"></param>
        /// <returns></returns>
        public async Task AddImageToProperty(PropertyImage newImage)
        {
            await _context.AddAsync(newImage);
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// Get Property image by id
        /// </summary>
        /// <param name="propertyImageId">id entity</param>
        /// <returns></returns>
        public async Task<PropertyImage> GetPropertyImageById(int propertyImageId)
        {
            return await _context.PropertyImages.FirstOrDefaultAsync(x => x.IdPropertyImage == propertyImageId);
        }
    }
}
