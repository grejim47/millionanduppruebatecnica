﻿using Microsoft.EntityFrameworkCore;
using MillionAndUp.Domain.DBEntities;

namespace MillionAndUp.Persistence.Context
{

    public class PropertiesContext : DbContext
    {
        // Define a DbSet property for each entity in your database
        /// <summary>
        /// Properties entity
        /// </summary>
        public DbSet<Property> Properties { get; set; }
        /// <summary>
        /// Owners entity
        /// </summary>
        public DbSet<Owner> Owners { get; set; }
        /// <summary>
        /// Property Images entity
        /// </summary>
        public DbSet<PropertyImage> PropertyImages { get; set; }
        /// <summary>
        /// Property traces entity
        /// </summary>
        public DbSet<PropertyTrace> PropertyTraces { get; set; }

        public PropertiesContext(DbContextOptions<PropertiesContext> options) : base(options)
        {
            // Constructor that receives the context configuration options
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Define additional configurations for your entities here, if necessary

            modelBuilder.Entity<PropertyTrace>()
            .HasOne(p => p.Property)
            .WithMany(c => c.PropertyTraces)
            .HasForeignKey(p => p.IdProperty);

            modelBuilder.Entity<Property>()
            .HasOne(p => p.Owner)
            .WithMany(c => c.Properties)
            .HasForeignKey(p => p.IdOwner);

            modelBuilder.Entity<PropertyImage>()
            .HasOne(p => p.Property)
            .WithMany(c => c.PropertyImages)
            .HasForeignKey(p => p.IdProperty);

            //modelBuilder.Entity<Owner>()
            //.HasMany(p => p.Properties)
            //.WithOne(c => c.Owner)
            //.HasForeignKey(p => p.IdOwner);
        }
    }
}
