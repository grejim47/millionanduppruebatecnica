﻿using Bogus;
using MillionAndUp.Domain.DBEntities;
using MillionAndUp.Domain.Request;
using MillionAndUp.Domain.Request.Properties;
using MillionAndUp.Domain.Response;
using MillionAndUp.Domain.Response.Properties;
using System.Net;

namespace MillionAndUp.Tests.Extensions
{
    public static class ApplicationExtensions
    {
        public static GetPropertiesRequest CreateRequestProperties(this Faker<GetPropertiesRequest> faker, string value = "test value")
        {
            Faker<Filters> filters = new();
            filters
                .RuleFor(x => x.Field, filters => "Name")
                .RuleFor(x => x.Value, filters => value)
                .RuleFor(x => x.Operation, filters => "equal");

            faker
                .RuleFor(x => x.Filters, faker => filters.Generate(1));
            var request = faker.Generate(1).FirstOrDefault();
            return request;
        }
        public static ResponseModel<List<GetPropertiesResponse>> CreateResponseProperties(this Faker<ResponseModel<List<GetPropertiesResponse>>> faker, string value = "test value")
        {
            string[] Fields = { "Name", "Address" };
            Faker<GetPropertiesResponse> filters = new();
            filters
                .RuleFor(x => x.Name, f => value)
                .RuleFor(x => x.Address, f => value)
                .RuleFor(x => x.CodeInternal, f => Guid.NewGuid().ToString())
                .RuleFor(x => x.IdProperty, f => f.Random.Number(1, 10))
                .RuleFor(x => x.Price, f => f.Random.Number(1000000, 2000000))
                .RuleFor(x => x.Year, f => f.Random.Number(2000, 2030))
                .RuleFor(x => x.IdOwner, f => f.Random.Number(1, 10));

            faker
                .RuleFor(x => x.Data, faker => filters.Generate(10))
                .RuleFor(x => x.Errors, faker => new List<ErrorsList>())
                .RuleFor(x => x.StatusCode, faker => HttpStatusCode.OK);
            var request = faker.Generate(1).FirstOrDefault();

            return request;
        }
        public static List<Property> CreatePropertyEntity(this Faker<Property> faker, string value = "test value", double price = 0)
        {
            faker
                .RuleFor(x => x.Name, f => value)
                .RuleFor(x => x.Address, f => value)
                .RuleFor(x => x.CodeInternal, f => Guid.NewGuid().ToString())
                .RuleFor(x => x.IdProperty, f => f.Random.Number(1, 10))
                .RuleFor(x => x.Price, f => price != 0 ? f.Random.Number(1000000, 2000000) : price)
                .RuleFor(x => x.Year, f => f.Random.Number(2000, 2030))
                .RuleFor(x => x.IdOwner, f => f.Random.Number(1, 10));

            var request = faker.Generate(10);
            return request;
        }
        public static ResponseModel<List<GetPropertiesResponse>> CreateResponsePropertiesNotFound(this Faker<ResponseModel<List<GetPropertiesResponse>>> faker)
        {
            faker
                .RuleFor(x => x.Data, faker => null)
                .RuleFor(x => x.Errors, faker => new List<ErrorsList>())
                .RuleFor(x => x.StatusCode, faker => HttpStatusCode.NotFound);
            var request = faker.Generate(1).FirstOrDefault();

            return request;
        }

        public static CreatePropertyRequest CreateRequestCreateProperties(this Faker<CreatePropertyRequest> faker)
        {
            faker
                .RuleFor(x => x.Address, f => "test value")
                .RuleFor(x => x.Name, f => "test value")
                .RuleFor(x => x.Price, f => f.Random.Int(100000, 200000))
                .RuleFor(x => x.Year, f => f.Random.Int(2000, 2030));
            var request = faker.Generate(1).FirstOrDefault();
            return request;
        }
        public static UpdatePropertyRequest CreateUpdatePropertiesRequest(this Faker<UpdatePropertyRequest> faker)
        {
            faker
                .RuleFor(x => x.Address, f => "new test value")
                .RuleFor(x => x.Name, f => "new test value")
                .RuleFor(x => x.IdProperty, f => f.Random.Int(1, 100));
            var request = faker.Generate(1).FirstOrDefault();
            return request;
        }
        public static UpdatePriceRequest CreateUpdatePropertiesPriceRequest(this Faker<UpdatePriceRequest> faker)
        {
            faker
                .RuleFor(x => x.Price, f => f.Random.Int(100000, 200000))
                .RuleFor(x => x.IdProperty, f => f.Random.Int(1, 100));
            var request = faker.Generate(1).FirstOrDefault();
            return request;
        }
    }
}
