﻿using AutoMapper;
using Bogus;
using MillionAndUp.Domain.DBEntities;
using MillionAndUp.Domain.Interfaces;
using MillionAndUp.Domain.Mapper;
using MillionAndUp.Domain.Request;
using MillionAndUp.Domain.Request.Properties;
using MillionAndUp.Domain.Response;
using MillionAndUp.Domain.Response.Properties;
using MillionAndUp.Services.Handlers;
using MillionAndUp.Tests.Extensions;
using Moq;
using System.Net;

namespace MillionAndUp.Tests.Application
{
    public class Application_tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void test_get_all_properties_accept()
        {
            GetPropertiesRequest request = new Faker<GetPropertiesRequest>().CreateRequestProperties();
            var response = new Faker<ResponseModel<List<GetPropertiesResponse>>>().CreateResponseProperties();

            //Maper
            //var configuration = new MapperConfiguration(cfg => cfg.AddProfile(new Mappers()));
            //IMapper mapper = new Mapper(configuration);

            //Mocks
            Mock<IProperties> _properties = new();
            _properties
                .Setup(p => p.GetProperties(request))
                .Returns(Task.FromResult(response));

            PropertiesHandlers handler = new(_properties.Object);
            var resultTask = handler.Handle(request, new CancellationToken());
            var result = resultTask.GetAwaiter().GetResult();

            Assert.Greater(result.Data.Count, 0);
        }
        [Test]
        public void test_get_all_properties_not_accept()
        {
            GetPropertiesRequest request = new Faker<GetPropertiesRequest>().CreateRequestProperties();
            ///0 records in the search
            var response = new Faker<ResponseModel<List<GetPropertiesResponse>>>().CreateResponsePropertiesNotFound();

            //Maper
            //var configuration = new MapperConfiguration(cfg => cfg.AddProfile(new Mappers()));
            //IMapper mapper = new Mapper(configuration);

            //Mocks
            Mock<IProperties> _properties = new();
            _properties
                .Setup(p => p.GetProperties(request))
                .Returns(Task.FromResult(response));

            PropertiesHandlers handler = new(_properties.Object);
            var resultTask = handler.Handle(request, new CancellationToken());
            var result = resultTask.GetAwaiter().GetResult();

            Assert.AreEqual(result.StatusCode, HttpStatusCode.NotFound);
        }


        [Test]
        public void test_create_properties_accept()
        {
            CreatePropertyRequest request = new Faker<CreatePropertyRequest>().CreateRequestCreateProperties();
            //var response = new Faker<ResponseModel<List<GetPropertiesResponse>>>().CreateResponseProperties();

            //Maper
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(new Mappers()));
            IMapper mapper = new Mapper(configuration);

            var entityProperty = mapper.Map<Property>(request);
            var response = new ResponseModel<GetPropertiesResponse>(mapper.Map<GetPropertiesResponse>(entityProperty));

            //Mocks
            Mock<IProperties> _properties = new();
            _properties
                .Setup(p => p.CreateNewProperty(request))
                .Returns(Task.FromResult(response));

            PropertiesHandlers handler = new(_properties.Object);
            var resultTask = handler.Handle(request, new CancellationToken());
            var result = resultTask.GetAwaiter().GetResult();

            Assert.NotNull(result.Data);
        }

        [Test]
        public void test_update_properties_accept()
        {
            string initValue = "test value";
            double initValuePrice = 100;
            UpdatePropertyRequest request = new Faker<UpdatePropertyRequest>().CreateUpdatePropertiesRequest();
            var entityModelFakeList = new Faker<Property>().CreatePropertyEntity(value: initValue, price: initValuePrice);
            var fakeEntityDb = entityModelFakeList.First();
            //Maper
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(new Mappers()));
            IMapper mapper = new Mapper(configuration);

            var entityProperty = mapper.Map(request, fakeEntityDb);
            var response = new ResponseModel<GetPropertiesResponse>(mapper.Map<GetPropertiesResponse>(entityProperty));

            //Mocks
            Mock<IProperties> _properties = new();
            _properties
                .Setup(p => p.UpdateProperty(request))
                .Returns(Task.FromResult(response));

            PropertiesHandlers handler = new(_properties.Object);
            var resultTask = handler.Handle(request, new CancellationToken());
            var result = resultTask.GetAwaiter().GetResult();

            Assert.AreEqual(entityProperty.Price, result.Data.Price);
            Assert.AreNotEqual(initValue, result.Data.Name);
        }
        [Test]
        public void test_update_properties_price_accept()
        {
            string initValue = "test value";
            double initValuePrice = 100;
            UpdatePriceRequest request = new Faker<UpdatePriceRequest>().CreateUpdatePropertiesPriceRequest();
            var entityModelFakeList = new Faker<Property>().CreatePropertyEntity(value: initValue, price: initValuePrice);
            var fakeEntityDb = entityModelFakeList.First();
            //Maper
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(new Mappers()));
            IMapper mapper = new Mapper(configuration);

            var entityProperty = mapper.Map(request, fakeEntityDb);
            var response = new ResponseModel<GetPropertiesResponse>(mapper.Map<GetPropertiesResponse>(entityProperty));

            //Mocks
            Mock<IProperties> _properties = new();
            _properties
                .Setup(p => p.UpdatePropertyPrice(request))
                .Returns(Task.FromResult(response));

            PropertiesHandlers handler = new(_properties.Object);
            var resultTask = handler.Handle(request, new CancellationToken());
            var result = resultTask.GetAwaiter().GetResult();

            Assert.AreEqual(initValue, result.Data.Name);
            Assert.AreNotEqual(initValuePrice, result.Data.Price);
        }

        [Test]
        public void test_filter_method_accept()
        {
            ///Generate filters with "test value" string in value for search
            GetPropertiesRequest request = new Faker<GetPropertiesRequest>().CreateRequestProperties("test value");
            ///Generate data with "test value" string in fields
            var response = new Faker<ResponseModel<List<GetPropertiesResponse>>>().CreateResponseProperties("test value");
            ///Testing filters
            var filterData = response.Data.AsQueryable().Filter(request.Filters);

            Assert.Greater(filterData.Count(), 0);
        }
        [Test]
        public void test_filter_method_not_accept()
        {
            ///Generate filters with "test value diferent" string in value for search
            GetPropertiesRequest request = new Faker<GetPropertiesRequest>().CreateRequestProperties("test value diferent");
            ///Generate data with "test value" string in fields
            var response = new Faker<ResponseModel<List<GetPropertiesResponse>>>().CreateResponseProperties("test value");
            ///Testing filters
            var filterData = response.Data.AsQueryable().Filter(request.Filters);

            Assert.AreEqual(0, filterData.Count());
        }
    }
}
