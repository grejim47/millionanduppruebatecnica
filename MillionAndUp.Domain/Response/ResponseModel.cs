﻿using Newtonsoft.Json;
using System.Net;

namespace MillionAndUp.Domain.Response
{
    /// <summary>
    /// Custom response model for all rest request
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ResponseModel<T>
    {
        [JsonIgnore]
        public HttpStatusCode StatusCode { get; set; }
        public List<ErrorsList> Errors { get; set; }
        public T Data { get; set; }
        public ResponseModel(T data)
        {
            StatusCode = HttpStatusCode.OK;
            Data = data;
            Errors = new List<ErrorsList>();
        }
        public ResponseModel(HttpStatusCode statusCode)
        {
            StatusCode = statusCode;
            Data = default;
            Errors = new List<ErrorsList>();
        }
        public ResponseModel(List<ErrorsList> errors)
        {
            StatusCode = HttpStatusCode.BadRequest;
            Data = default;
            Errors = errors;
        }
        public ResponseModel(Exception exception)
        {
            StatusCode = HttpStatusCode.InternalServerError;
            Data = default;
            Errors = new List<ErrorsList>();
        }
        public ResponseModel()
        {
        }

    }

    public class ErrorsList
    {
        public string PropertyName { get; set; }
        public string ErrorMessage { get; set; }
    }
}
