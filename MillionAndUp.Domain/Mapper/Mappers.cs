﻿using AutoMapper;
using MillionAndUp.Domain.DBEntities;
using MillionAndUp.Domain.Request.Properties;
using MillionAndUp.Domain.Response.Properties;

namespace MillionAndUp.Domain.Mapper
{
    /// <summary>
    /// Mappers
    /// </summary>
    public class Mappers : Profile
    {
        public Mappers()
        {
            ///Map Property model to GetPropertiesResponse
            CreateMap<Property, GetPropertiesResponse>();
            ///Map UpdatePropertyRequest model to Property
            CreateMap<UpdatePropertyRequest, Property>();
            ///Map UpdatePriceRequest model to Property
            CreateMap<UpdatePriceRequest, Property>();
            ///Map CreatePropertyRequest model to Property
            CreateMap<CreatePropertyRequest, Property>()
                .ForMember(x => x.CodeInternal, f => f.MapFrom(z => Guid.NewGuid().ToString()))
                .ForMember(x => x.IdOwner, f => f.MapFrom(z => 1));
        }
    }
}
