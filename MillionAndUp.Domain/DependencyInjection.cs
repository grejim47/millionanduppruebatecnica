﻿using Microsoft.Extensions.DependencyInjection;
using MillionAndUp.Domain.Mapper;

namespace MillionAndUp.Domain
{
    public static class DependencyInjection
    {
        /// <summary>
        /// Add domain services
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddDomain(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(Mappers));
            return services;
        }
    }
}