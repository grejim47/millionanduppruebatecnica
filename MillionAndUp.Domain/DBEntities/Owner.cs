﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MillionAndUp.Domain.DBEntities
{
    /// <summary>
    /// Owner entity
    /// </summary>
    [Table("Owner")]
    public class Owner
    {
        [Key]
        public int IdOwner { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        [JsonIgnore]
        public byte[] Photo { get; set; }
        public DateTime Birthday { get; set; }
        [JsonIgnore]
        public List<Property> Properties { get; set; }
    }
}
