﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MillionAndUp.Domain.DBEntities
{
    [Table("PropertyImage")]
    public class PropertyImage
    {
        [Key]
        public int IdPropertyImage { get; set; }
        [ForeignKey("IdProperty")]
        public int IdProperty { get; set; }
        [JsonIgnore]
        public byte[] File { get; set; }
        public bool Enable { get; set; }
        [JsonIgnore]
        public Property Property { get; set; }
        [NotMapped]
        public string UrlDownload { get; set; }

        public PropertyImage(int idProperty, byte[] file)
        {
            IdProperty = idProperty;
            File = file;
            Enable = true;
        }
    }
}
