﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MillionAndUp.Domain.DBEntities
{
    [Table("Property")]
    public class Property
    {
        [Key]
        public int IdProperty { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public double Price { get; set; }
        public string CodeInternal { get; set; }
        public int Year { get; set; }
        public int IdOwner { get; set; }
        public Owner Owner { get; set; }
        public List<PropertyTrace> PropertyTraces { get; set; }
        public List<PropertyImage> PropertyImages { get; set; }
    }
}
