﻿using Microsoft.AspNetCore.Mvc;
using MillionAndUp.Domain.FluentValidation;

namespace MillionAndUp.Domain.Interfaces
{
    public interface IServices
    {
        /// <summary>
        /// Execute Methods with Json Response
        /// </summary>
        /// <typeparam name="T">ValidateRequest FluentValidator</typeparam>
        /// <param name="Request">request</param>
        /// <returns></returns>
        Task<ObjectResult> Execute<T>(T Request) where T : ValidateRequest;
        /// <summary>
        /// Execute Methods with Files Response
        /// </summary>
        /// <typeparam name="T">ValidateRequest FluentValidator</typeparam>
        /// <param name="Request">request</param>
        /// <returns></returns>
        Task<IActionResult> FileExecute<T>(T Request) where T : ValidateRequest;
    }
}
