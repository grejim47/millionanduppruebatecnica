﻿using MillionAndUp.Domain.DBEntities;

namespace MillionAndUp.Domain.Interfaces
{
    public interface IPropertyImagePersistence
    {
        /// <summary>
        /// AddImage to property
        /// </summary>
        /// <param name="newImage"></param>
        /// <returns></returns>
        Task AddImageToProperty(PropertyImage newImage);
        /// <summary>
        /// Get Property image by id
        /// </summary>
        /// <param name="propertyImageId">id entity</param>
        /// <returns></returns>
        Task<PropertyImage> GetPropertyImageById(int propertyImageId);
    }
}
