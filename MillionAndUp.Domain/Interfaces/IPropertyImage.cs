﻿using MillionAndUp.Domain.Request.Properties;
using MillionAndUp.Domain.Response;

namespace MillionAndUp.Domain.Interfaces
{
    public interface IPropertyImage
    {
        /// <summary>
        /// Add new image to property
        /// </summary>
        /// <param name="request">request</param>
        /// <returns></returns>
        Task<ResponseModel<string>> AddImageToProperty(AddImagePropertyRequest request);
        /// <summary>
        /// Add new image to property
        /// </summary>
        /// <param name="request">request</param>
        /// <returns></returns>
        Task<(byte[], string, string)?> DownloadPropertyImage(DownloadPropertyImageRequest request);
    }
}
