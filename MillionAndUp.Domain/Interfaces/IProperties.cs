﻿using MillionAndUp.Domain.Request.Properties;
using MillionAndUp.Domain.Response;
using MillionAndUp.Domain.Response.Properties;

namespace MillionAndUp.Domain.Interfaces
{
    /// <summary>
    /// Properties interface
    /// </summary>
    public interface IProperties
    {
        /// <summary>
        /// Get properties by filters
        /// </summary>
        /// <param name="request">filters</param>
        /// <returns>properties by filters</returns>
        Task<ResponseModel<List<GetPropertiesResponse>>> GetProperties(GetPropertiesRequest request);
        /// <summary>
        /// Update property
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>updated property</returns>
        Task<ResponseModel<GetPropertiesResponse>> UpdateProperty(UpdatePropertyRequest request);
        /// <summary>
        /// Update property price
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>updated property</returns>
        Task<ResponseModel<GetPropertiesResponse>> UpdatePropertyPrice(UpdatePriceRequest request);
        /// <summary>
        /// Create a new property
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>property created</returns>
        Task<ResponseModel<GetPropertiesResponse>> CreateNewProperty(CreatePropertyRequest request);
    }
}
