﻿using MillionAndUp.Domain.DBEntities;
using MillionAndUp.Domain.Request;

namespace MillionAndUp.Domain.Interfaces
{
    public interface IPropertiesPersistence
    {
        /// <summary>
        /// Get properties by filters
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        Task<List<Property>> GetProperties(List<Filters> filters);
        /// <summary>
        /// Get property by Id
        /// </summary>
        /// <param name="idProperty">id property</param>
        /// <returns>Property</returns>
        Task<Property> GetPropertyById(int idProperty);
        /// <summary>
        /// Update property
        /// </summary>
        /// <param name="propertyToUpdate">Property to update</param>
        /// <returns></returns>
        Task UpdateProperty(Property propertyToUpdate);
        /// <summary>
        /// Create property
        /// </summary>
        /// <param name="newProperty">newProperty</param>
        /// <returns></returns>
        Task AddNewProperty(Property newProperty);
    }
}
