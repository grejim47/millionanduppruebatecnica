﻿using System.Linq.Expressions;

namespace MillionAndUp.Domain.Request
{
    public class RequestFilter
    {
        /// <summary>
        /// Filters
        /// </summary>
        public List<Filters> Filters { get; set; }
    }

    public static class FiltersUtils
    {
        /// <summary>
        /// Dynamic Filter for search in entity framework
        /// </summary>
        /// <typeparam name="T">Entity or list IQuerable</typeparam>
        /// <typeparam name="Q">List of filters</typeparam>
        /// <param name="source">Data source</param>
        /// <param name="request">Filters</param>
        /// <returns></returns>
        public static IQueryable<T> Filter<T, Q>(this IQueryable<T> source, Q request) where Q : List<Filters>
        {
            List<Filters> filter = request;

            if (filter == null || !filter.Any())
                return source;
            ParameterExpression ParameterTypes = Expression.Parameter(typeof(T), "x");
            List<Expression> contentExpression = new List<Expression>();

            foreach (var itemFilter in filter)
            {
                Expression left = Expression.Property(ParameterTypes, itemFilter.Field);
                //Expression right = Expression.Constant(itemFilter.Value);
                Expression right = Expression.Constant(Convert.ChangeType(itemFilter.Value, left.Type));


                switch (itemFilter.Operation)
                {
                    case "equal":
                        contentExpression.Add(Expression.Equal(left, right));
                        break;
                    case "in":
                        var method = left.Type.GetMethod("Contains", new[] { left.Type });
                        contentExpression.Add(Expression.Call(left, method, right));
                        break;
                }
            }

            if (contentExpression.Any())
            {
                Expression consolidado = null;
                foreach (var item in contentExpression)
                {
                    if (consolidado is null)
                        consolidado = item;
                    else
                        consolidado = Expression.And(consolidado, item);
                }
                var result = Expression.Lambda<Func<T, bool>>(consolidado, new List<ParameterExpression>() { ParameterTypes });
                return source.Where(result);
            }
            return source;
        }
    }
}
