﻿using FluentValidation;
using System.Reflection;

namespace MillionAndUp.Domain.Request
{
    /// <summary>
    /// Filter model
    /// </summary>
    public class Filters
    {
        /// <summary>
        /// Field of search
        /// </summary>
        public string Field { get; set; }
        /// <summary>
        /// value of search
        /// </summary>
        public dynamic Value { get; set; }
        /// <summary>
        /// operation equals or in operators
        /// </summary>
        public string Operation { get; set; }
    }
    /// <summary>
    /// Rules for validate request
    /// </summary>
    /// <typeparam name="T">request</typeparam>
    internal class FiltersValidator<T> : AbstractValidator<IEnumerable<Filters>>
    {
        /// <summary>
        /// Custom Rules
        /// </summary>
        public FiltersValidator()
        {
            string[] operators = { "equal", "in" };
            PropertyInfo[] properties = typeof(T).GetProperties();

            RuleForEach(x => x).ChildRules(filter =>
            {
                filter.RuleFor(x => x.Operation).NotNull().WithMessage("El campo Operation no puede ser null");
                filter.RuleFor(x => x.Operation).NotEmpty().WithMessage("El campo Operation no puede estar vacio");

                filter.RuleFor(x => x.Field).NotNull().WithMessage("El campo Field no puede ser null");
                filter.RuleFor(x => x.Field).NotEmpty().WithMessage("El campo Field no puede estar vacio");

                filter.RuleFor(x => x.Value).NotNull().WithMessage("El campo Value no puede ser null");

                filter.RuleFor(x => x.Operation).Must(x => operators.Contains(x)).WithMessage($"El campo Operation solo acepta los valores: {String.Join(",", operators)}");
                filter.RuleFor(x => x).Must(x => ValidateOperator(x)).WithMessage($"El campo value de tipo numerico o booleano no puede tener el operador in");
                filter.RuleFor(x => x.Field).Must(x => properties.Any(z => String.Equals(z.Name, x, StringComparison.OrdinalIgnoreCase))).WithMessage($"El campo Field solo acepta los valores: {String.Join(",", properties.Select(x => x.Name))}");

            });
            /// Validate operator and value
            static bool ValidateOperator(Filters filter)
            {
                if (filter.Operation == "in")
                {
                    var typeValue = filter.Value.GetType();
                    Type[] numericTypes = { typeof(int), typeof(long), typeof(short), typeof(float), typeof(double), typeof(decimal), typeof(bool), typeof(Boolean) };
                    foreach (Type type in numericTypes)
                    {
                        if (type == typeValue)
                            return false;
                    }
                }
                return true;
            }
        }
    }
}
