﻿using FluentValidation;
using FluentValidation.Results;
using MediatR;
using MillionAndUp.Domain.DBEntities;
using MillionAndUp.Domain.FluentValidation;
using MillionAndUp.Domain.Response;
using MillionAndUp.Domain.Response.Properties;

namespace MillionAndUp.Domain.Request.Properties
{
    /// <summary>
    /// GetProperties request
    /// </summary>
    public class GetPropertiesRequest : ValidateRequest, IRequest<ResponseModel<List<GetPropertiesResponse>>>
    {
        /// <summary>
        /// Filters
        /// </summary>
        public List<Filters> Filters { get; set; }
        /// <summary>
        /// define fluent validator
        /// </summary>
        /// <returns></returns>
        public override ValidationResult FluentValidateRequest() => base.Validate<ValidateRequest, GetPropertiesValidator>(this);
    }
    /// <summary>
    /// Rules of request
    /// </summary>
    internal class GetPropertiesValidator : AbstractValidator<GetPropertiesRequest>
    {
        /// <summary>
        /// Rules
        /// </summary>
        public GetPropertiesValidator()
        {
            RuleFor(x => x.Filters).SetValidator(new FiltersValidator<Property>());
        }
    }
}
