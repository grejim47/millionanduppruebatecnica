﻿using FluentValidation;
using FluentValidation.Results;
using MediatR;
using MillionAndUp.Domain.FluentValidation;
using MillionAndUp.Domain.Response;
using MillionAndUp.Domain.Response.Properties;

namespace MillionAndUp.Domain.Request.Properties
{
    /// <summary>
    /// Update Property Price request
    /// </summary>
    public class UpdatePriceRequest : ValidateRequest, IRequest<ResponseModel<GetPropertiesResponse>>
    {
        /// <summary>
        /// idProperty to update
        /// </summary>
        public int IdProperty { get; set; }
        /// <summary>
        /// new price
        /// </summary>
        public double Price { get; set; }
        public override ValidationResult FluentValidateRequest() => base.Validate<UpdatePriceRequest, UpdatePriceValidator>(this);

    }
    /// <summary>
    /// Rules of request UpdatePriceRequest
    /// </summary>
    internal class UpdatePriceValidator : AbstractValidator<UpdatePriceRequest>
    {
        /// <summary>
        /// define custom rules
        /// </summary>
        public UpdatePriceValidator()
        {
            RuleFor(x => x.IdProperty).NotNull().WithMessage("El campo IdProperty no puede ser null");
            RuleFor(x => x.IdProperty).NotEmpty().WithMessage("El campo IdProperty no puede estar vacio");

            RuleFor(x => x.Price).NotEmpty().WithMessage("El campo Price no puede estar vacio");
            RuleFor(x => x.Price).LessThanOrEqualTo(100000000).WithMessage("El campo Price no puede exceder los $100.000.000");
        }
    }
}
