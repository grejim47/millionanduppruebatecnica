﻿using FluentValidation;
using FluentValidation.Results;
using MediatR;
using MillionAndUp.Domain.FluentValidation;

namespace MillionAndUp.Domain.Request.Properties
{
    /// <summary>
    /// request for dowload image of property
    /// </summary>
    public class DownloadPropertyImageRequest : ValidateRequest, IRequest<(byte[], string, string)?>
    {
        /// <summary>
        /// Idproperty image to download
        /// </summary>
        public int IdPropertyImage { get; set; }
        public override ValidationResult FluentValidateRequest() => base.Validate<DownloadPropertyImageRequest, DownloadPropertyImageValidator>(this);

        public DownloadPropertyImageRequest(int idPropertyImage)
        {
            IdPropertyImage = idPropertyImage;
        }
    }
    /// <summary>
    /// custom rules for DownloadPropertyImageRequest
    /// </summary>
    internal class DownloadPropertyImageValidator : AbstractValidator<DownloadPropertyImageRequest>
    {
        /// <summary>
        /// Rules
        /// </summary>
        public DownloadPropertyImageValidator()
        {
            RuleFor(x => x.IdPropertyImage).NotNull().WithMessage("El campo IdPropertyImage no puede ser null");
            RuleFor(x => x.IdPropertyImage).NotEmpty().WithMessage("El campo IdPropertyImage no puede estar vacio");
        }
    }
}
