﻿using FluentValidation;
using FluentValidation.Results;
using MediatR;
using Microsoft.AspNetCore.Http;
using MillionAndUp.Domain.FluentValidation;
using MillionAndUp.Domain.Response;

namespace MillionAndUp.Domain.Request.Properties
{
    /// <summary>
    /// AddImagePropertyRequest
    /// </summary>
    public class AddImagePropertyRequest : ValidateRequest, IRequest<ResponseModel<string>>
    {
        /// <summary>
        /// Id property to add a new image
        /// </summary>
        public int IdProperty { get; set; }
        /// <summary>
        /// File
        /// </summary>
        public IFormFile Archivo { get; set; }
        public override ValidationResult FluentValidateRequest() => base.Validate<AddImagePropertyRequest, AddImagePropertyValidator>(this);

    }
    /// <summary>
    /// custom validation rules for addImagePropertyRequest
    /// </summary>
    internal class AddImagePropertyValidator : AbstractValidator<AddImagePropertyRequest>
    {
        /// <summary>
        /// Rules
        /// </summary>
        public AddImagePropertyValidator()
        {
            RuleFor(x => x.IdProperty).NotNull().WithMessage("El campo IdProperty no puede ser null");
            RuleFor(x => x.IdProperty).NotEmpty().WithMessage("El campo IdProperty no puede estar vacio");

            RuleFor(x => x.Archivo).NotNull().WithMessage("El Archivo no puede ser null");
            RuleFor(x => x.Archivo.Length).LessThan(3000000).WithMessage("El Archivo no puede superar los 10mb");
            RuleFor(x => x.Archivo.ContentType).Must(x => x == "image/png").WithMessage("El Archivo debe ser de tipo .png");
        }
    }
}
