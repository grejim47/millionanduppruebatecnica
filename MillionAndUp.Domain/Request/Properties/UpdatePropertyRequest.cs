﻿using FluentValidation;
using FluentValidation.Results;
using MediatR;
using MillionAndUp.Domain.FluentValidation;
using MillionAndUp.Domain.Response;
using MillionAndUp.Domain.Response.Properties;

namespace MillionAndUp.Domain.Request.Properties
{
    /// <summary>
    /// Update property request
    /// </summary>
    public class UpdatePropertyRequest : ValidateRequest, IRequest<ResponseModel<GetPropertiesResponse>>
    {
        /// <summary>
        /// Id property to update
        /// </summary>
        public int IdProperty { get; set; }
        /// <summary>
        /// new Name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// new adrress
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// new price
        /// </summary>
        //public double Price { get; set; }

        /// <summary>
        /// Define fluent validator
        /// </summary>
        /// <returns></returns>
        public override ValidationResult FluentValidateRequest() => base.Validate<UpdatePropertyRequest, UpdatePropertyValidator>(this);
    }
    /// <summary>
    /// Rules for update property request
    /// </summary>
    internal class UpdatePropertyValidator : AbstractValidator<UpdatePropertyRequest>
    {
        /// <summary>
        /// define custom rules
        /// </summary>
        public UpdatePropertyValidator()
        {
            RuleFor(x => x.IdProperty).NotNull().WithMessage("El campo IdProperty no puede ser null");
            RuleFor(x => x.IdProperty).NotEmpty().WithMessage("El campo IdProperty no puede estar vacio");

            RuleFor(x => x.Name).NotEmpty().WithMessage("El campo Name no puede estar vacio");
            RuleFor(x => x.Name).MaximumLength(50).WithMessage("El campo Name no puede exceder los 50 caracteres");

            RuleFor(x => x.Address).NotEmpty().WithMessage("El campo Address no puede estar vacio");
            RuleFor(x => x.Address).MaximumLength(50).WithMessage("El campo Address no puede exceder los 50 caracteres");

            //RuleFor(x => x.Price).NotEmpty().WithMessage("El campo Price no puede estar vacio");
            //RuleFor(x => x.Price).LessThanOrEqualTo(100000000).WithMessage("El campo Price no puede exceder los $100.000.000");
        }
    }
}
