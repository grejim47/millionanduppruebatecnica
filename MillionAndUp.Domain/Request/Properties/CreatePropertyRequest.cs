﻿using FluentValidation;
using FluentValidation.Results;
using MediatR;
using MillionAndUp.Domain.FluentValidation;
using MillionAndUp.Domain.Response;
using MillionAndUp.Domain.Response.Properties;

namespace MillionAndUp.Domain.Request.Properties
{
    public class CreatePropertyRequest : ValidateRequest, IRequest<ResponseModel<GetPropertiesResponse>>
    {
        /// <summary>
        /// Name of new property
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Address of new property
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// Price of new property
        /// </summary>
        public double Price { get; set; }
        /// <summary>
        /// Year of new property
        /// </summary>
        public int Year { get; set; }
        public override ValidationResult FluentValidateRequest() => base.Validate<CreatePropertyRequest, CreatePropertyValidator>(this);
    }
    /// <summary>
    /// Custom rules for validate createpropertyRequest model
    /// </summary>
    internal class CreatePropertyValidator : AbstractValidator<CreatePropertyRequest>
    {
        /// <summary>
        /// Custom rules
        /// </summary>
        public CreatePropertyValidator()
        {
            RuleFor(x => x.Name).NotNull().WithMessage("El campo Name no puede ser null");
            RuleFor(x => x.Name).NotEmpty().WithMessage("El campo Name no puede estar vacio");
            RuleFor(x => x.Name).MaximumLength(50).WithMessage("El campo Name no puede superar los 50 caracteres");

            RuleFor(x => x.Address).NotNull().WithMessage("El campo Address no puede ser null");
            RuleFor(x => x.Address).NotEmpty().WithMessage("El campo Address no puede estar vacio");
            RuleFor(x => x.Name).MaximumLength(50).WithMessage("El campo Address no puede superar los 50 caracteres");

            RuleFor(x => x.Price).NotNull().WithMessage("El campo Price no puede ser null");
            RuleFor(x => x.Price).NotEmpty().WithMessage("El campo Price no puede estar vacio");
            RuleFor(x => x.Price).LessThanOrEqualTo(100000000).WithMessage("El campo Price no puede exceder los $100.000.000");

            RuleFor(x => x.Year).NotNull().WithMessage("El campo Year no puede ser null");
            RuleFor(x => x.Year).NotEmpty().WithMessage("El campo Year no puede estar vacio");
            RuleFor(x => x.Year).LessThanOrEqualTo(2050).WithMessage("El campo Year no puede exceder el año 2050");
            RuleFor(x => x.Year).GreaterThanOrEqualTo(1900).WithMessage("El campo Year no puede ser menor a el año 1900");
        }
    }
}
