﻿using FluentValidation;
using FluentValidation.Results;

namespace MillionAndUp.Domain.FluentValidation
{
    public abstract class ValidateRequest
    {
        /// <summary>
        /// Abstract method
        /// </summary>
        /// <returns></returns>
        public abstract ValidationResult FluentValidateRequest();
        /// <summary>
        /// Validate request by fluent validator
        /// </summary>
        /// <typeparam name="TRequest">request</typeparam>
        /// <typeparam name="TValidator">validator context</typeparam>
        /// <param name="request">request</param>
        /// <returns>ValidationResult</returns>
        public ValidationResult Validate<TRequest, TValidator>(TRequest request) where TValidator : IValidator
        {
            var validator = (TValidator)Activator.CreateInstance(typeof(TValidator));
            var context = new ValidationContext<TRequest>(request);
            return validator.Validate(context);
        }
    }
}
