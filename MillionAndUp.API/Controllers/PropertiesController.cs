﻿using Microsoft.AspNetCore.Mvc;
using MillionAndUp.API.Auth;
using MillionAndUp.Domain.Interfaces;
using MillionAndUp.Domain.Request.Properties;
using MillionAndUp.Domain.Response;
using MillionAndUp.Domain.Response.Properties;
using System.Web.Http.Filters;

namespace MillionAndUp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(BasicAuthFilterAttribute))]
    public class PropertiesController : ControllerBase
    {
        private readonly IServices _api;

        public PropertiesController(IServices api)
        {
            _api = api;
        }

        /// <summary>
        /// Get all properties
        /// </summary>
        /// <returns>all properties</returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseModel<GetPropertiesResponse>), 200)]
        [Route("CreateProperty")]
        public async Task<IActionResult> CreateProperty(CreatePropertyRequest request) => await _api.Execute(request);

        /// <summary>
        /// Get all properties
        /// </summary>
        /// <returns>all properties</returns>
        [HttpGet]
        [ProducesResponseType(typeof(ResponseModel<List<GetPropertiesResponse>>), 200)]
        [Route("GetAllProperties")]
        public async Task<IActionResult> GetAllProperties() => await _api.Execute(new GetPropertiesRequest());

        /// <summary>
        /// Get properties by filters
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>list of properties</returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseModel<List<GetPropertiesResponse>>), 200)]
        [Route("GetProperties")]
        public async Task<IActionResult> GetProperties(GetPropertiesRequest request) => await _api.Execute(request);

        /// <summary>
        /// Update property
        /// </summary>
        /// <returns>property updated</returns>
        [HttpPut]
        [ProducesResponseType(typeof(ResponseModel<GetPropertiesResponse>), 200)]
        [Route("UpdateProperty")]
        public async Task<IActionResult> GetAllProperties(UpdatePropertyRequest request) => await _api.Execute(request);

        /// <summary>
        /// Update price
        /// </summary>
        /// <returns>property updated</returns>
        [HttpPut]
        [ProducesResponseType(typeof(ResponseModel<GetPropertiesResponse>), 200)]
        [Route("UpdatePrice")]
        public async Task<IActionResult> UpdatePrice(UpdatePriceRequest request) => await _api.Execute(request);

        /// <summary>
        /// Add image to property
        /// </summary>
        /// <returns>property updated</returns>
        [HttpPut]
        [ProducesResponseType(typeof(ResponseModel<string>), 200)]
        [Route("AddImageProperty")]
        public async Task<IActionResult> AddImageProperty([FromForm] AddImagePropertyRequest request) => await _api.Execute(request);

        /// <summary>
        /// Download image
        /// </summary>
        /// <returns>property updated</returns>
        [HttpGet]
        [ProducesResponseType(typeof(FileResult), 200)]
        [Route("DownloadPropertyImage")]
        public async Task<IActionResult> DownloadPropertyImage(int idPropertyImage) => await _api.FileExecute(new DownloadPropertyImageRequest(idPropertyImage));
    }
}
