﻿using Microsoft.AspNetCore.Mvc;
using MillionAndUp.Domain.Interfaces;

namespace MillionAndUp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OwnerController : ControllerBase
    {
        private readonly IServices _api;

        public OwnerController(IServices api)
        {
            _api = api;
        }
    }
}
