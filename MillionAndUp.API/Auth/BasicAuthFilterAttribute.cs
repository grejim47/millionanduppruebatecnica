﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System.Text;

namespace MillionAndUp.API.Auth
{
    public class BasicAuthFilterAttribute : ActionFilterAttribute
    {
        public override async void OnActionExecuting(ActionExecutingContext actionContext)
        {
            // Verificar si se proporciona el encabezado de autorización
            if (String.IsNullOrEmpty(actionContext.HttpContext.Request.Headers.Authorization))
            {
                actionContext.Result = new UnauthorizedObjectResult("Unauthorized");
                return;
            }

            // Extraer las credenciales del encabezado de autorización
            string authHeader = actionContext.HttpContext.Request.Headers.Authorization;
            byte[] credentialsBytes = Convert.FromBase64String(authHeader.Replace("Basic ", ""));
            string[] credentials = Encoding.ASCII.GetString(credentialsBytes).Split(':');
            string username = credentials[0];
            string password = credentials[1];

            
            if (!IsUserAuthenticated(username, password))
            {
                actionContext.Result = new UnauthorizedObjectResult("Unauthorized");
                return;
            }
        }

        private bool IsUserAuthenticated(string username, string password)
        {
            //Custom credentials
            return username == "user" && password == "pass";
        }
    }
}
