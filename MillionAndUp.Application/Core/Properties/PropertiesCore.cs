﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using MillionAndUp.Domain.DBEntities;
using MillionAndUp.Domain.Interfaces;
using MillionAndUp.Domain.Request.Properties;
using MillionAndUp.Domain.Response;
using MillionAndUp.Domain.Response.Properties;

namespace MillionAndUp.Application.Core.Properties
{
    public class PropertiesCore : IProperties
    {
        private readonly IPropertiesPersistence _propertiesPersistence;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public PropertiesCore(IPropertiesPersistence propertiesPersistence, IMapper mapper, IHttpContextAccessor httpContextAccessor)
        {
            _propertiesPersistence = propertiesPersistence;
            _mapper = mapper;
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// Get properties per any filter
        /// </summary>
        /// <param name="request">filters</param>
        /// <returns>List of properties</returns>
        public async Task<ResponseModel<List<GetPropertiesResponse>>> GetProperties(GetPropertiesRequest request)
        {
            ResponseModel<List<GetPropertiesResponse>> result;
            try
            {
                var properties = await _propertiesPersistence.GetProperties(request.Filters);
                if (properties.Any())
                {
                    var listMap = _mapper.Map<List<GetPropertiesResponse>>(properties);
                    FillUrlDownload(listMap);
                    result = new ResponseModel<List<GetPropertiesResponse>>(listMap);
                }
                else
                    result = new ResponseModel<List<GetPropertiesResponse>>(System.Net.HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                result = new ResponseModel<List<GetPropertiesResponse>>(ex);
            }
            return result;
        }
        /// <summary>
        /// Update property
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>updated property</returns>
        public async Task<ResponseModel<GetPropertiesResponse>> UpdateProperty(UpdatePropertyRequest request)
        {
            ResponseModel<GetPropertiesResponse> result;
            try
            {
                var propertyDB = await _propertiesPersistence.GetPropertyById(request.IdProperty);
                if (propertyDB is not null)
                {
                    ///Merge and send to update
                    var updateMap = _mapper.Map(request, propertyDB);
                    await _propertiesPersistence.UpdateProperty(updateMap);
                    var response = _mapper.Map<GetPropertiesResponse>(updateMap);
                    FillUrlDownload(response);
                    result = new ResponseModel<GetPropertiesResponse>(response);
                }
                else
                    result = new ResponseModel<GetPropertiesResponse>(System.Net.HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                result = new ResponseModel<GetPropertiesResponse>(ex);
            }
            return result;
        }
        /// <summary>
        /// Update property price
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>updated property</returns>
        public async Task<ResponseModel<GetPropertiesResponse>> UpdatePropertyPrice(UpdatePriceRequest request)
        {
            ResponseModel<GetPropertiesResponse> result;
            try
            {
                var propertyDB = await _propertiesPersistence.GetPropertyById(request.IdProperty);
                if (propertyDB is not null)
                {
                    ///Merge and send to update
                    var updateMap = _mapper.Map(request, propertyDB);
                    await _propertiesPersistence.UpdateProperty(updateMap);
                    var response = _mapper.Map<GetPropertiesResponse>(updateMap);
                    FillUrlDownload(response);
                    result = new ResponseModel<GetPropertiesResponse>(response);
                }
                else
                    result = new ResponseModel<GetPropertiesResponse>(System.Net.HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                result = new ResponseModel<GetPropertiesResponse>(ex);
            }
            return result;
        }
        /// <summary>
        /// Update property price
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>updated property</returns>
        public async Task<ResponseModel<GetPropertiesResponse>> CreateNewProperty(CreatePropertyRequest request)
        {
            ResponseModel<GetPropertiesResponse> result;
            try
            {
                ///Map request to property entity model
                var entityProperty = _mapper.Map<Property>(request);
                if (entityProperty is not null)
                {
                    await _propertiesPersistence.AddNewProperty(entityProperty);
                    var response = _mapper.Map<GetPropertiesResponse>(entityProperty);
                    FillUrlDownload(response);
                    result = new ResponseModel<GetPropertiesResponse>(response);
                }
                else
                    result = new ResponseModel<GetPropertiesResponse>(System.Net.HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                result = new ResponseModel<GetPropertiesResponse>(ex);
            }
            return result;
        }
        #region Aux Methods
        /// <summary>
        /// Get url of API
        /// </summary>
        /// <returns></returns>
        private string ObtenerUrlBase()
        {
            try
            {
                var request = _httpContextAccessor.HttpContext.Request;
                var baseUrl = $"{request.Scheme}://{request.Host}";
                return baseUrl;
            }
            catch (Exception)
            {
                return "{baseUrl}";
            }
        }
        /// <summary>
        /// Fill url download field in response
        /// </summary>
        /// <param name="listMap"></param>
        void FillUrlDownload(List<GetPropertiesResponse> listMap)
        {
            string urlProject = ObtenerUrlBase();
            foreach (var item in listMap)
            {
                if (item.PropertyImages is not null && item.PropertyImages.Any())
                    item.PropertyImages = item.PropertyImages.Where(x => x.Enable && x.File != null && x.File.Length > 0).Select(z => { z.UrlDownload = $"{urlProject}/api/Properties/DownloadPropertyImage/?idPropertyImage={z.IdPropertyImage}"; return z; }).ToList();
            }
        }
        /// <summary>
        /// Fill url download field in response
        /// </summary>
        /// <param name="listMap"></param>
        void FillUrlDownload(GetPropertiesResponse property)
        {
            string urlProject = ObtenerUrlBase();
            if (property.PropertyImages is not null && property.PropertyImages.Any())
                property.PropertyImages = property.PropertyImages.Where(x => x.Enable && x.File != null && x.File.Length > 0).Select(z => { z.UrlDownload = $"{urlProject}/api/Properties/DownloadPropertyImage/?idPropertyImage={z.IdPropertyImage}"; return z; }).ToList();
        }
        #endregion
    }
}
