﻿using MillionAndUp.Domain.DBEntities;
using MillionAndUp.Domain.Interfaces;
using MillionAndUp.Domain.Request.Properties;
using MillionAndUp.Domain.Response;

namespace MillionAndUp.Application.Core.Properties
{
    /// <summary>
    /// PropertyImage core
    /// </summary>
    public class PropertyImageCore : IPropertyImage
    {
        private readonly IPropertyImagePersistence _imagePropertyPersistence;
        private readonly IPropertiesPersistence _propertiesPersistence;

        public PropertyImageCore(IPropertyImagePersistence imagePropertyPersistence, IPropertiesPersistence propertiesPersistence)
        {
            _imagePropertyPersistence = imagePropertyPersistence;
            _propertiesPersistence = propertiesPersistence;
        }
        /// <summary>
        /// Add new image to property
        /// </summary>
        /// <param name="request">request</param>
        /// <returns></returns>
        public async Task<ResponseModel<string>> AddImageToProperty(AddImagePropertyRequest request)
        {
            ResponseModel<string> result;
            try
            {
                var propertyDB = await _propertiesPersistence.GetPropertyById(request.IdProperty);
                if (propertyDB is not null)
                {
                    byte[] archivoBytes;
                    using (var memoryStream = new MemoryStream())
                    {
                        await request.Archivo.CopyToAsync(memoryStream);
                        archivoBytes = memoryStream.ToArray();
                    }
                    var newImage = new PropertyImage(request.IdProperty, archivoBytes);
                    await _imagePropertyPersistence.AddImageToProperty(newImage);
                    result = new ResponseModel<string>("Image Added");
                }
                else
                    result = new ResponseModel<string>(System.Net.HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                result = new ResponseModel<string>(ex);
            }
            return result;
        }
        /// <summary>
        /// Add new image to property
        /// </summary>
        /// <param name="request">request</param>
        /// <returns></returns>
        public async Task<(byte[], string, string)?> DownloadPropertyImage(DownloadPropertyImageRequest request)
        {
            (byte[], string, string)? result = null;
            try
            {
                var propertyDB = await _imagePropertyPersistence.GetPropertyImageById(request.IdPropertyImage);
                if (propertyDB is not null)
                {
                    result = (propertyDB.File, "image/png", $"{propertyDB.IdPropertyImage}.png");
                }
                else
                    result = null;
            }
            catch (Exception ex)
            {

            }
            return result;
        }
    }
}
