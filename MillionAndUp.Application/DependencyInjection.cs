﻿using Microsoft.Extensions.DependencyInjection;
using MillionAndUp.Application.Core.Properties;
using MillionAndUp.Domain.Interfaces;

namespace MillionAndUp.Application
{
    public static class DependencyInjection
    {
        /// <summary>
        /// add aplication services
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddScoped<IProperties, PropertiesCore>();
            services.AddScoped<IPropertyImage, PropertyImageCore>();
            return services;
        }
    }
}